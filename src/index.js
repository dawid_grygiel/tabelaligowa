import React from "react"
import ReactDOM from "react-dom"
import "./style.css"
import Header from "./components/header"
import MainContent from "./components/MainContent"

ReactDOM.render(<div>
    <Header />
    <MainContent />
</div>, document.getElementById("root"))
