import React from "react"
import JsonTable from "./jsonTable"

let jsonObject = {}

class MainContent extends React.Component{
    constructor(){
        super()
        this.state = {
            warning: '',
            jsonLoaded: false,
            jsonObject: {},
            team1Points: null,
            team2Points: null,
            teamA: -1,
            teamB: -1,
        }
        this.loadJSON = this.loadJSON.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.addPoints = this.addPoints.bind(this)
    }

    loadJSON(){
        let reader = new FileReader()
        let fileFromInput = document.getElementById('fileInput').files
        let jsonFile = ''

            if(fileFromInput[0].name.split('.').pop() === 'json'){
                this.setState({warning: ""})
                reader.readAsText(fileFromInput[0])
                reader.onload = () => {
                    jsonFile = reader.result
                    if(jsonFile.length > 0){
                        try {
                            jsonObject = JSON.parse(jsonFile)
                        }catch(err){
                            this.setState({warning: err})
                        }
                    }else{
                        this.setState({warning: "Plik JSON jest pusty"})
                        return 0
                    }

                    try {
                        jsonObject.League.Team.forEach((element) => {
                            for(const value in element){
                                if(value !== 'Name'){
                                    element[value] = parseInt(element[value])
                                }
                            }
                        })
                        this.setState({jsonObject: jsonObject, jsonLoaded: true})
                    }catch(error){
                        this.setState({warning: "plik JSON jest zle skonstruowany"})
                    }
                }
            }else{
                this.setState({warning: "to nie jest plik JSON"})
            }
    }

            handleChange(event){
                let {name, value} = event.target
                this.setState({[name]: value})
            }

            sortTable(table){
                table.sort((a, b) => {
                    if (a.Points !== b.points ){
                        return b.points - a.points
                    }else if(a.Wins !== b.Wins){
                        return b.Wins - a.Wins
                    }else {
                        return b.LostGoals - a.LostGoals
                    }
                })
            }

            addPoints(event){
                event.preventDefault()
                let jsonUpdatedTeams = jsonObject
                let {teamA, teamB, team1Points, team2Points } = this.state
                //jezeli teamA i teamB beda rozne i nie nedzie wybrana opcja "wybierz druzyne"
                if(teamA!== teamB && teamA !== -1 && teamB !== -1){
                    //jezeli w punktach nie bedzie liter
                    if(/^\d+$/.test(team1Points) && /^\d+$/.test(team2Points)){
                        team1Points = parseInt(team1Points)
                        team2Points = parseInt(team2Points)
                    }
                    // jezeli punkty beda typem liczb
                    if (typeof team1Points === `number` && typeof team2Points === 'number'){

                        jsonUpdatedTeams.League.Team[teamA].MatchCount += 1
                        jsonUpdatedTeams.League.Team[teamB].MatchCount += 1                    
                        jsonUpdatedTeams.League.Team[teamA].ScoredGoals += team1Points
                        jsonUpdatedTeams.League.Team[teamA].LostGoals += team2Points
                        jsonUpdatedTeams.League.Team[teamB].ScoredGoals += team2Points
                        jsonUpdatedTeams.League.Team[teamB].LostGoals += team1Points
                        if(team1Points > team2Points){
                            jsonUpdatedTeams.League.Team[teamA].Wins += 1
                            jsonUpdatedTeams.League.Team[teamB].Losts += 1
                        }else if(team1Points === team2Points){
                            jsonUpdatedTeams.League.Team[teamA].Draws += 1
                            jsonUpdatedTeams.League.Team[teamB].Draws += 1
                        }else{
                            jsonUpdatedTeams.League.Team[teamB].Wins += 1
                            jsonUpdatedTeams.League.Team[teamA].Losts += 1
                        }
                        this.sortTable(jsonUpdatedTeams.League.Team)
                        this.setState({jsonObject: jsonUpdatedTeams,
                            team1Points: '',
                            team2Points: '',
                            warning: '',
                            teamA: -1,
                            teamB: -1,
                        })
                    }else this.setState({warning: "Złe typy danych"})
                }else this.setState({warning: "Drużyny/a są nie wybrane lub takie same"})
        }
    render(){
        return ( 
            <div>{ this.state.jsonLoaded ?
                        <JsonTable
                            Click={this.addPoints} 
                            data={this.state.jsonObject}
                            TeamAIndex={this.state.teamA}
                            TeamBIndex={this.state.teamB}
                            teamAPoints={this.state.team1Points}
                            teamBPoints={this.state.team2Points}
                            onUpdate={this.handleChange}
                            warning={this.state.warning}
                        />
                    :
                    <form action="POST" name="file" id="uploadFile">
                        <input type="file" onChange={this.loadJSON} id="fileInput" name="jsonFile" accept=".json" />
                        <label id="loadButton" htmlFor="fileInput">Wczytaj Drużyny</label>
                        <div className="warning">{this.state.warning}</div>
                    </form>
                }
            </div>
        )
    }
}

export default MainContent