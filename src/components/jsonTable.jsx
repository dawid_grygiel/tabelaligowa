import React from "react"

function JsonTable(props){
    return(<main>
        {/* {poczatek tabeli} */}
        <table>
            <tr className="title">
                <td className="ordinary" >Lp.</td>
                <td className="longTitle">Nazwa drużyny</td>
                <td className="ordinary" >Ilość Meczy</td>
                <td className="ordinary" >Zwycięstwa</td>
                <td className="ordinary" >Remisy</td>
                <td className="ordinary" >Porażki</td>
                <td className="ordinary" >B. strzelone</td>
                <td className="ordinary" >B. stracone</td>
                <td className="ordinary" >Punkty</td>
            </tr>
            {props.data.League.Team.map( (i, j) => {
                return(
                    <tr key={j+1} className="otherTr">
                            <td  className="green" >{j+1}</td>
                            <td  className="longTeam">{i.Name}</td>
                            <td  className="normal">{i.MatchCount}</td>
                            <td  className="normal">{i.Wins}</td>
                            <td  className="normal">{i.Draws}</td>
                            <td  className="normal">{i.Losts}</td>
                            <td  className="normal">{i.ScoredGoals}</td>
                            <td  className="normal">{i.LostGoals}</td>
                            <td  className="normal">{(i.Wins*3) + (i.Draws)}</td>
                    </tr>
                )
            })}
        </table>
        {/* {koniec tabeli} */}

        {/* {poczatek inputow dodawania punktow} */}
        <form action="#"  id="addScore">
        <fieldset>

            <select name="teamA" value={props.TeamAIndex} onChange={props.onUpdate} id="selectTeamA">

                {/* {opcja wylaczona resetowana przy dodaniu punktow} */}
                <option value={`-1`} disabled selected>Wybierz drużynę</option>

                    {props.data.League.Team.map((i,j) => {
                        return(
                            <option value={`${j}`}>{i.Name}</option>
                        )
                    })}

            </select>

        <input type="number" onChange={props.onUpdate} value={props.teamAPoints} name="team1Points" className="floatright" min="0" placeholder="Bramki drużyny wybranej" id="TeamPoints" />
        
        </fieldset>

        <fieldset>

            <select name="teamB" value={props.TeamBIndex} onChange={props.onUpdate} id="selectTeamB">

                {/* {opcja wylaczona resetowana przy dodaniu punktow} */}
                <option value={`-1`} disabled selected>Wybierz drużynę</option>

                    {props.data.League.Team.map((i,j) => {
                        return(
                            <option value={`${j}`}>{i.Name}</option>
                        )
                    })}

            </select>

            <input type="number" onChange={props.onUpdate} value={props.teamBPoints} name="team2Points" className="floatright" min="0" placeholder="Bramki drużyny przeciwnej" id="CompetitionPoints" />
        
        </fieldset>

        <fieldset>
            <button id="submitScore"  onClick={props.Click} className="clearfix">Wprowadź wynik</button>
        </fieldset>

    </form>
    {/* {koniec inputow dodawania punktow} */}

    {/* {div z mozliwym warningiem} */}
    <div className="warning">{props.warning}</div>
    
    </main>)}

export default JsonTable